#!/usr/bin/env python3
import os
import sys

file=sys.argv[1]

filename = os.path.basename(file);

output= filename + ".png"

size = os.path.getsize(file);

pixels = int(size / 3);
width = int(pixels ** 0.5);
height = int(pixels / width);
# print(width)


os.system("convert -depth 8 -size {}x{} rgb:{} {}.png"
    .format(width, height, file, os.path.basename(file)));
os.system("montage -label {} {} -frame 5 -geometry +0+0 -background Gold {}"
    .format(filename, output, output))