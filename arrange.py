#!/usr/bin/env python3
import os
from PIL import Image
import numpy as np
import bisect

imgwidth = 2048
imgheight = 2048

output = Image.new("RGB", (imgwidth, imgheight))

files = os.listdir("./pngs")

files.sort(key=lambda file: os.path.getsize("./pngs/" + file), reverse=True)

class Region:
    def __init__(self, box):
        x0, y0, x1, y1 = box
        self.x0 = int(x0)
        self.x1 = int(x1)
        self.y0 = int(y0)
        self.y1 = int(y1)
    def offset(self, offset):
        w = self.width() # these aren't usable during the following, as
        h = self.height() # they depend on x0 and x1,
            # so we have to calculate and store them now.
        self.x0 = self.x0 + offset.x0 + int((offset.width()  - w)  / 2)
        self.x1 = self.x1 + offset.x0 + int((offset.width()  - w) / 2)
        self.y0 = self.y0 + offset.y0 + int((offset.height() - h)/ 2)
        self.y1 = self.y1 + offset.y0 + int((offset.height() - h) / 2)
    def subtract(self, removed):
        return [i for i in
            [Region((self.x0, self.y0, self.x1, removed.y0)),
            Region((self.x0, self.y0, removed.x0, self.y1)),
            Region((removed.x1, self.y0, self.x1, self.y1)),
            Region((self.x0, removed.y1, self.x1, self.y1))]
            if i.notEmpty()]
    def notEmpty(self):
        return self.width() > 0 and self.height() > 0
    def width(self):
        return self.x1 - self.x0
    def height(self):
        return self.y1 - self.y0
    def toImageBox(self):
        return (self.x0, self.y0, self.x1, self.y1)
    def canContain(self, other):
        return self.width() >= other.width() and self.height() >= other.height()
    def overlaps(self, other):
        return not(self.x0 >= other.x1 or self.x1 <= other.x0 or self.y0 >= other.y1 or self.y1 <= other.y0)
    def size(self):
        return self.width() * self.height()
    def __str__(self):
        return str((self.x0, self.y0, self.x1, self.y1))
    def __repr__(self):
        return str((self.x0, self.y0, self.x1, self.y1))
    def __lt__(self, other):
        return self.size() < other.size()

squares = [Region((0, 0, imgwidth, imgheight))]

sum = 0

smallestSize = 0

largestSize = 1024 ** 2

for file in files:
    sum = sum + 1
    # print(file)
    im: Image = Image.open("./pngs/" + file)
    # print(im.size)
    box = Region((0, 0, im.width, im.height))
    print(box)
    # files.sort(key=lambda file: os.path.getsize("./pngs/" + file), reverse=False)
    fitting = [i for i in squares if i.canContain(box)]
    if len(fitting) == 0 or box.size() > largestSize:
        print("no space for {}".format(file))
        print(sum)
        continue
    region = fitting[0]
    box.offset(region)
    output.paste(im, box.toImageBox())
    for fit in fitting:
        if fit.overlaps(box):
            for i in fit.subtract(box):
                bisect.insort(squares, i)
    squares = [i for i in squares if not i.overlaps(box) and i.size() >= smallestSize]
    # squares = [square for square in squares if len([i for i in squares if i.canContain(square)]) == 1]
    #squares.sort(reverse=True)
    #squares = squares[-100000:]
    if len(squares) == 0:
        print("no space left")
        break
    print(len(squares))
    print(sum)

output.save("out.png")